#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from importlib.metadata import version


# read version from installed package
__version__ = version("vitese")
