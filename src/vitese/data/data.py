#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path
from typing import List, Tuple

DATA_DIR = Path(__file__).parent.parent / 'data' / 'raw'


def parse_filename(fname: str) -> Tuple[str, bool]:
    if fname.endswith('_wo_transcript.txt'):
        lang = fname.rsplit('_', 2)[0]
        has_transcript = False
    else:
        lang = fname.rsplit('.')[0]
        has_transcript = True
    return lang, has_transcript


def get_data_configs() -> List[Tuple[str, bool]]:
    fpaths = DATA_DIR.glob('*.txt')
    return [parse_filename(f.name) for f in fpaths]


def get_filepath(lang: str, with_transcript: bool = True) -> Path:
    suffix = '' if with_transcript else '_wo_transcript'
    return DATA_DIR / f'{lang}{suffix}.txt'


def get_video_ids(lang: str, only_with_transcript: bool = False) -> List[str]:
    data_configs = get_data_configs()

    found_with_transcript = (lang, True) in data_configs
    found_wo_transcript = (lang, False) in data_configs

    if only_with_transcript:
        if not found_with_transcript:
            raise ValueError(f"There is no {lang} data with {lang} transcript.")
    else:
        if not found_with_transcript and not found_wo_transcript:
            raise ValueError(f"There is no {lang} data.")

    if found_with_transcript:
        fpath = get_filepath(lang, True)
    else:
        fpath = get_filepath(lang, False)

    video_ids = []
    with fpath.open('rt') as fin:
        for ln in fin:
            video_ids.append(ln.strip())
    return video_ids
