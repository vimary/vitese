#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from typing import List, Dict

from youtube_transcript_api import YouTubeTranscriptApi


def get_transcript(video_id: str, lang: str, manual: bool = False) -> List[Dict]:
    transcripts = YouTubeTranscriptApi.list_transcripts(video_id)
    if manual:
        # filter for manually created lang transcripts
        transcript = transcripts.find_manually_created_transcript([lang])
        return transcript.fetch()
    else:
        # filter for lang transcripts
        transcript = transcripts.find_transcript([lang])
        return transcript.fetch()


if __name__ == "__main__":
    from vitese.data import data

    video_ids = data.get_video_ids('ge')
    for v_id in video_ids:
        print(f'Getting {v_id}\'s replicas...')
        replicas = get_transcript(v_id, 'ru')
        print('Top fetched replicas:')
        for r in replicas[:5]:
            print(f'{json.dumps(r, indent=2)}')
