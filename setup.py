#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

import setuptools

ROOT_PACKAGE_DIR = Path(__file__).parent.resolve()


def read_requirements():
    """Parses requirements from requirements.txt"""
    with (ROOT_PACKAGE_DIR / 'requirements.txt').open('r', encoding='utf-8') as fin:
        reqs = [line.strip() for line in fin if not line.strip().startswith('#')]
    with (ROOT_PACKAGE_DIR / 'dev-requirements.txt').open('r', encoding='utf-8') as fin:
        dev_reqs = [line.strip() for line in fin if not line.strip().startswith('#')]
    return {'install_requires': reqs, 'extras_require': {'dev': dev_reqs}}


def read_readme():
    with (ROOT_PACKAGE_DIR / 'README.md').open('r', encoding='utf-8') as fin:
        long_description = fin.read()
    return long_description


setuptools.setup(name='vitese',
                 version='0.1.0',
                 description="Tool to extract audio/video with phrases containing"
                 " your words in Georgian",
                 long_description=read_readme(),
                 long_description_content_type='text/markdown',
                 url='https://gitlab.com/vimary/vitese.git',
                 author='Mariia Trofimova',
                 author_email='mary.vikhreva@gmail.com',
                 package_dir={'': 'src'},
                 packages=setuptools.find_packages('src'),
                 include_package_data=True,
                 classifiers=[
                     'Programming Language :: Python :: 3'
                 ],
                 python_requires=">=3.8",
                 **read_requirements())
